package dv.spring.kotlin.service

import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl : SelectedProductService{
    override fun getSelectedProductWithPage(name:String,page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductDao.getSelectedProductWithPage(name,page,pageSize)
    }

    @Autowired
    lateinit var selectedProductDao:SelectedProductDao
    override fun getSelectedProduct(): List<SelectedProduct> {
        return selectedProductDao.getSelectedProduct()
    }

}