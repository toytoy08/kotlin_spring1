package dv.spring.kotlin.entity.dto

data class ShoppingCartInfoDto(

//        Customer name
        var customerName: String? = null,

//        Customer Default Address
        var defaultAddress: AddressDto? = null,

//        Product
        var productData: List<ProductInfoDto>? = null
)

//Data add
//Customername = Lung
//Product = Predator Helios 500, Switch3, Notebook 9
//Quantity = 2 ,3 ,4