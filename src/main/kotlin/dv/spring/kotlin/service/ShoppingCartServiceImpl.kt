package dv.spring.kotlin.service

import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import dv.spring.kotlin.entity.dto.ShoppingCartInfoDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ShoppingCartServiceImpl : ShoppingCartService {
    @Transactional
    override fun save(customerId: Long, shoppingCartInfoDto: ShoppingCartInfoDto): ShoppingCart {
        val customer = customerDao.findById(customerId)
        val shoppingCart = ShoppingCart(status = ShoppingCartStatus.WAIT)

        for (each in shoppingCartInfoDto.productData!!) {
            var product = each.id?.let { productDao.findById(it) }
            var selectedProduct = SelectedProduct(product, each.quantity)
            selectedProduct.product = product
            selectedProductDao.save(selectedProduct)
            shoppingCart.selectedProducts.add(selectedProduct)
        }

        shoppingCart.customer = customer
        shoppingCartDao.save(shoppingCart)

        return shoppingCart
    }

    override fun getShoppingCartsByStatus(status: ShoppingCartStatus): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsByStatus(status)
    }

    override fun getShoppingCartsPartialWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsPartialWithPage(name, page, pageSize)
    }

    override fun getShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartsWithPage(page, pageSize)
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao
    @Autowired
    lateinit var productDao: ProductDao
    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }
}