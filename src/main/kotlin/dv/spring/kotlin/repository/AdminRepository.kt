package dv.spring.kotlin.repository

import dv.spring.kotlin.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface AdminRepository : CrudRepository<JwtUser, Long>{
    fun findByUsername(username: String): JwtUser
}