package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageShoppingCartsDto
import dv.spring.kotlin.entity.dto.ProductInfoDto
import dv.spring.kotlin.entity.dto.ShoppingCartDto
import dv.spring.kotlin.entity.dto.ShoppingCartInfoDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController

class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getShoppingCarts(): ResponseEntity<Any> {
        val shopppingCart = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shopppingCart))
    }

    @GetMapping("/shoppingcart/pageconfig")
    fun getShoppingCartsWithPage(
            @RequestParam("page") page: Int,
            @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartsWithPage(page, pageSize)
        return ResponseEntity.ok(PageShoppingCartsDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCart = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @GetMapping("/shoppingcart/nameandpage")
    fun getShoppingCartsPartialWithPage(
            @RequestParam("name") name: String,
            @RequestParam("page") page: Int,
            @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartsPartialWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageShoppingCartsDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingCart = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @PostMapping("/shoppingcart/addcustomer/{customerId}")
    fun addShoppingCart(@PathVariable customerId: Long,
                        @RequestBody shoppingCartInfoDto: ShoppingCartInfoDto): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCartService.save(customerId, shoppingCartInfoDto))
        val outputDto = mutableListOf<ProductInfoDto>()
        for (each in output.selectedProducts!!) {
            outputDto.add(ProductInfoDto(
                    name = each.product?.name,
                    description = each.product?.description,
                    quantity = each?.quantity,
                    id = each.product?.id
            ))
        }
        return ResponseEntity.ok(ShoppingCartInfoDto(
                customerName = output.customer?.name,
                defaultAddress = output.customer?.defaultAddress,
                productData = outputDto
        ))
    }
}