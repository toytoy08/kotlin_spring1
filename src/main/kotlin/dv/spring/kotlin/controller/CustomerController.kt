package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController

class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @GetMapping("/customer")
    fun getCustomers(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("customer/query")
    fun getCustomers(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customer/partialQuery")
    fun getCustomerPartial(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialName(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/NameOrEmailQuery")
    fun getCustomerPartialAndEmail(@RequestParam("name") name: String,
                                   @RequestParam(value = "email", required = false) email: String?): ResponseEntity<Any> {
        var output: List<CustomerDto> = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialNameAndEmail(name, name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/AddressCustomer")
    fun getCustomerByAddress(@RequestParam("address") province: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByAddress(province))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/userstatus")
    fun getCustomerByStatus(@RequestParam("status") status: UserStatus): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByStatus(status))
        return ResponseEntity.ok(output)
    }

    @GetMapping("customer/product")
    fun getCustomerFromBoughtProduct(@RequestParam(name = "name") name: String): ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.findByBoughtProduct(name))
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto): ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/defaultaddress/{addressId}")
    fun addCustomer(@RequestBody customerDto: CustomerDto,
                    @PathVariable addressId: Long): ResponseEntity<Any> {
        val output = customerService.save(
                addressId,
                MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id: Long): ResponseEntity<Any> {
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the customer id is not found")
    }
}