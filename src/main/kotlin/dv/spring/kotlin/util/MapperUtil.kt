package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import dv.spring.kotlin.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer", target = "manu")
    )

    fun mapProductDto(product: Product?): ProductDto?
    fun mapProductDto(products: List<Product>): List<ProductDto>
    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto
    fun mapManufacturer(manulist: List<Manufacturer>): List<ManufacturerDto>
    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    @Mappings(
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
    )
    fun mapCustomerWithAuthDto(customer: Customer?): CustomerWithAuthDto

    fun mapCustomerDto(customer: Customer?): CustomerDto
    fun mapCustomerDto(customers: List<Customer>): List<CustomerDto>
    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<Authority>

    fun mapAddressDto(address: Address): AddressDto
    fun mapAddressDto(address: List<Address>): List<AddressDto>
    @InheritInverseConfiguration
    fun mapAddressDto(addressDto: AddressDto): Address

    fun mapShoppingCartDto(shoppingCart: ShoppingCart): ShoppingCartDto
    fun mapShoppingCartDto(shoppingCarts: List<ShoppingCart>): List<ShoppingCartDto>
    @InheritInverseConfiguration
    fun mapShoppingCartDto(shoppingCartDto: ShoppingCartDto): ShoppingCart

    fun mapSelectedProductDto(selectedProduct: SelectedProduct): SelectedProductDto
    fun mapSelectedProductDto(selectedProduct: List<SelectedProduct>): List<SelectedProductDto>

//    @InheritInverseConfiguration
//    fun mapAdminDto(adminDto: AdminDto): Admin
//    fun mapAdminDto(admin: Admin): AdminDto
//    fun mapAdminDto(admin: List<Admin>): List<Admin>
}