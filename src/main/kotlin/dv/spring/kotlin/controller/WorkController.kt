//package dv.spring.kotlin.controller
//
//import dv.spring.kotlin.entity.Product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//
//class WorkController {
//    @GetMapping("/getAppName")
//    fun getAppName(): String {
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun getProduct(): ResponseEntity<Any> {
//        val product = Product("iPhone", "A new telephone", 28000, 5)
//        return ResponseEntity.ok(product)
//    }
//
//    @GetMapping("/product/{name}")
//    fun getPathParam(@PathVariable("name") name: String): ResponseEntity<Any> {
//        val product = Product(name, "A new telephone", 28000, 5)
//        if (name == "iPhone") {
//            return ResponseEntity.ok(product)
//        } else {
//            return ResponseEntity.notFound().build()
//        }
//    }
//
//    @PostMapping("/setZeroQuantity")
//    fun postSetZeroQuantity(@RequestBody product: Product): ResponseEntity<Any> {
//        product.quantity = 0
//        return ResponseEntity.ok(product)
//    }
//
//    @PostMapping("/totalPrice")
//    fun postTotalPrice(@RequestBody product: List<Product>): String {
//        var sum = 0
//        for (i in product.indices) {
//            sum += product[i].price
//        }
//        return "$sum"
//    }
//
//    @PostMapping("/avaliableProduct")
//    fun avaliableProduct(@RequestBody product: List<Product>): ResponseEntity<Any> {
//        var output = mutableListOf<Product>()
//        for (i in product.indices){
//            if ( product[i].quantity != 0)
//            {
//                output.add(product[i])
//            }
//        }
//        return ResponseEntity.ok(output)
//    }
//}