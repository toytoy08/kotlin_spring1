package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl : CustomerService {
    @Transactional
    override fun remove(id: Long): Customer {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    @Transactional
    override fun save(customer: Customer): Customer {
        val address = customer.defaultAddress?.let { addressDao.save(it) }
        val customer = customerDao.save(customer)
        return customer
    }

    @Transactional
    override fun save(addressId: Long, customer: Customer): Customer {
        val address = addressDao.findById(addressId)
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        return customer
    }

    @Autowired
    lateinit var addressDao: AddressDao

    override fun getCustomerByStatus(status: UserStatus): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }

    override fun getCustomerByAddress(province: String): List<Customer> {
        return customerDao.getCustomerByAddress(province)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomerByName(name: String): Customer? = customerDao.getCustomerByName(name)

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun findByBoughtProduct(name: String): List<Customer?> {
        return shoppingCartDao.findByProductName(name)
                .map { shoppingCart -> shoppingCart.customer }
                .toSet()
                .toList()
    }
}

