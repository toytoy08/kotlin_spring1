package dv.spring.kotlin.entity

data class Person(var name:String,var surname:String,var age:Int)

data class MyPerson(var name:String,var surname:String,var team :String,var age:Int)