package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository: CrudRepository<ShoppingCart,Long>{
    fun findAll(page: Pageable): Page<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase (name:String,page: Pageable): Page<ShoppingCart>
    fun findByStatus(status: ShoppingCartStatus) : List<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase (name: String):List<ShoppingCart>
//    fun findByCustomer_Id(id:Long):ShoppingCart
}