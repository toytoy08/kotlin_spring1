package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.UserStatus

data class CustomerDto(
        var name: String? = null,
        var email: String? = null,
//        var username: String? = null,
        var userStatus: UserStatus? = UserStatus.PENDING,
        var defaultAddress: AddressDto? = null,
        var id: Long? = null
)