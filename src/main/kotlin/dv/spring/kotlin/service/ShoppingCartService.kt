package dv.spring.kotlin.service

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import dv.spring.kotlin.entity.dto.ShoppingCartInfoDto
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsPartialWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByStatus(status: ShoppingCartStatus): List<ShoppingCart>
    fun save(customerId: Long, shoppingCartInfoDto: ShoppingCartInfoDto): ShoppingCart
}
