package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import dv.spring.kotlin.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDaoDBImpl : ShoppingCartDao {
    override fun save(shoppingCart: ShoppingCart): ShoppingCart {
        return shoppingCartRepository.save(shoppingCart)
    }

    override fun findByProductName(name: String): List<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Product_NameContainingIgnoreCase(name)
    }


    override fun getShoppingCartsByStatus(status: ShoppingCartStatus): List<ShoppingCart> {
        return shoppingCartRepository.findByStatus(status)
    }

    override fun getShoppingCartsPartialWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Product_NameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    override fun getShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findAll(PageRequest.of(page, pageSize))
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}