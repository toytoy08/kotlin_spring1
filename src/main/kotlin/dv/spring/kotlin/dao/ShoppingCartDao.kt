package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.ShoppingCartStatus
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartsWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsPartialWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartsByStatus(status: ShoppingCartStatus): List<ShoppingCart>
    fun findByProductName(name: String): List<ShoppingCart>
    fun save(shoppingCart: ShoppingCart): ShoppingCart
}