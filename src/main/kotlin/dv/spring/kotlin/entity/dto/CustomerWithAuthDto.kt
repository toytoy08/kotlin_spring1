package dv.spring.kotlin.entity.dto

data class CustomerWithAuthDto(
        var name: String? = null,
        var email: String? = null,
        var username: String? = null,
        var authorities: List<AuthorityDto> = mutableListOf()
)