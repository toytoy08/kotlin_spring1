package dv.spring.kotlin.entity

import javax.persistence.Entity

@Entity
data class Admin(override var name: String? = null,
                 override var email: String? = null,
                 override var userStatus: UserStatus? = UserStatus.PENDING
) : User (name,email,userStatus){
}