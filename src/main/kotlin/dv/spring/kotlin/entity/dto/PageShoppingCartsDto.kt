package dv.spring.kotlin.entity.dto

data class PageShoppingCartsDto(var totalPages: Int? = null,
                                var totalElements: Long? = null,
                                var shoppingCart: List<ShoppingCartDto> = mutableListOf())