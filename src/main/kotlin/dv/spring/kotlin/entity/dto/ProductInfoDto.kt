package dv.spring.kotlin.entity.dto

data class ProductInfoDto(var name: String? = null,
                          var description: String? = null,
                          var quantity: Int? = null,
                          var id: Long? = null)