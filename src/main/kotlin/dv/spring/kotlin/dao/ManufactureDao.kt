package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer

interface ManufactureDao {
    fun getManufactueres(): List<Manufacturer>
    fun save(manufacturer: Manufacturer): Manufacturer
    fun findById(manuId: Long): Manufacturer?

}