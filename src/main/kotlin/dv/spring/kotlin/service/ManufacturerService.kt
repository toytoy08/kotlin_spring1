package dv.spring.kotlin.service

import dv.spring.kotlin.entity.dto.ManufacturerDto
import dv.spring.kotlin.entity.Manufacturer

interface ManufacturerService{
    fun getManufacturers():List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer
}