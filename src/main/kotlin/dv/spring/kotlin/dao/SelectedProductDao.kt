package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao{
    fun getSelectedProduct() : List<SelectedProduct>
    fun getSelectedProductWithPage(name:String,page: Int, pageSize: Int): Page<SelectedProduct>
    fun save(selectedProduct: SelectedProduct):SelectedProduct
}